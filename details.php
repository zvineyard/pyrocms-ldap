<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Ldap extends Module {

	public $version = '1.0';

	public function info()
	{
		return array(
			'name' => array(
				'en' => 'LDAP'
			),
			'description' => array(
				'en' => 'A module for LDAP authentication.'
			),
			'frontend' => false,
			'backend' => false,
		);
	}

	public function install()
	{
		$ldap_settings = array(
			array(
            	'slug' => 'ldap_host',
                'title' => 'LDAP Host',
                'description' => 'The IP address or host name of your LDAP server.',
                'type' => 'text',
                'default' => '',
                'value' => 'sol.nnu.edu',
                'options' => '',
                'is_required' => 0,
                'is_gui' => 1,
                'module' => 'ldap',
                'order' => 1,
            ),
			array(
            	'slug' => 'ldap_port',
                'title' => 'LDAP Port',
                'description' => 'The port number used by your LDAP server.',
                'type' => 'text',
                'default' => '',
                'value' => '389',
                'options' => '',
                'is_required' => 0,
                'is_gui' => 1,
                'module' => 'ldap',
                'order' => 2,
            ),
			array(
            	'slug' => 'ldap_domain',
                'title' => 'LDAP Domain',
                'description' => 'Your LDAP domain.',
                'type' => 'text',
                'default' => '',
                'value' => 'nampa.nnu',
                'options' => '',
                'is_required' => 0,
                'is_gui' => 1,
                'module' => 'ldap',
                'order' => 3,
            ),
			array(
            	'slug' => 'ldap_basedn',
                'title' => 'LDAP Base DN',
                'description' => 'The base DN for your LDAP queries. Separate multiple DNs with a pipe delimiter.',
                'type' => 'text',
                'default' => '',
                //'value' => 'ou=employees,dc=nampa,dc=nnu|ou=students,dc=nampa,dc=nnu',
                'value' => 'ou=employees,dc=nampa,dc=nnu',
                'options' => '',
                'is_required' => 0,
                'is_gui' => 1,
                'module' => 'ldap',
                'order' => 4,
            ),
            array(
                'slug' => 'ldap_group',
                'title' => 'LDAP User Group',
                'description' => 'The ID of the default group to which first-time logins will be assigned.',
                'type' => 'text',
                'default' => '',
                'value' => '2',
                'options' => '',
                'is_required' => 0,
                'is_gui' => 1,
                'module' => 'ldap',
                'order' => 5,
            )
		);

		if($this->db->insert_batch('settings', $ldap_settings))
		{
			return true;
		}
	}

	public function uninstall()
	{
		$this->db->delete('settings', array('module' => 'ldap'));
		return true;
	}


	public function upgrade($old_version)
	{
		// Your Upgrade Logic
		return true;
	}

	public function help()
	{
		// Return a string containing help info
		// You could include a file and return it here.
		return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
	}
}
/* End of file details.php */