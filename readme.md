# PyroCMS LDAP Module

This module allows you to use LDAP authentication. Once enabled, users will be able to use LDAP to login to the public site and to your site's control panel (i.e. yoursite.com/admin).

WARNING: You must overwrite a core system controller and a core module controller (provided in the repo) to get LDAP authentication to work. The controllers are found at:

	system/cms/controllers/admin.php
	system/cms/modules/users/controllers/users.php

_Make sure you back up both controllers!_

WARNING: Once you enable this module, you'll want to immediately add your LDAP settings to the settings area in PyroCMS, otherwise you may get locked out of your site's control panel. To look at more than one Base DN, seperate your DNs with a "|" (pipe) character.

This module has been tested on PyroCMS 2.2.1.