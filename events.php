<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Sample Events Class
 * 
 * @package     PyroCMS
 * @subpackage  Sample Module
 * @category    events
 * @author      PyroCMS Dev Team
 */
class Events_Ldap
{
    protected $ci;
    
    public function __construct()
    {
        $this->ci =& get_instance();
        
        // register the public_controller event when this file is autoloaded
        Events::register('third_party_login', array($this, 'run'));
     }
    
    // If this returns false, you won't be able to login to the control panel
    public function run()
    {
        // Get LDAP module settings
        $res = $this->ci->db->get_where(SITE_REF.'_settings', array('module' => 'ldap'))->result();

        foreach($res as $v)
        {
            $settings[$v->slug] = $v->value;
        }

        $this->ci->load->model('users/ion_auth_model');
        $this->ci->ion_auth_model->identity_column = "username";

        $ldap_logged_in = false;

        $user = $this->ci->input->post('email');
        $password = $this->ci->input->post('password');
        $host = $settings['ldap_host'];
        $port = $settings['ldap_port'];
        $domain = $settings['ldap_domain'];

        // Explode base_dn on pipe character
        $base_dn = @explode('|',$settings['ldap_basedn']);
        //echo '<pre>';
        //print_r($base_dn);
        //die();
        $group = $settings['ldap_group'];

        if(is_array($base_dn) && count($base_dn) > 0 && $base_dn != "" && $base_dn != NULL)
        {
            foreach($base_dn as $basedn)
            {
                $ad = ldap_connect($host,$port) or die('Could not connect to LDAP server.');
                ldap_set_option($ad, LDAP_OPT_PROTOCOL_VERSION, 3);
                ldap_set_option($ad, LDAP_OPT_REFERRALS, 0);

                // ldap_bind complains with a warning if credientials aren't right --- ignoring
                if(@ldap_bind($ad, "{$user}@{$domain}", $password))
                {
                    $attr = array('dn','displayname');
                    if ($result = ldap_search($ad, $basedn,"(&(objectClass=user)(objectCategory=person)(samaccountname={$user}))")) {
                        $info = ldap_get_entries($ad, $result);
                        
                        /*
                        echo '<pre>';
                        print_r($info);
                        die();
                        */
                        
                        if(isset($info[0]))
                        {
                            $first_name = $info[0]['givenname'][0];
                            $last_name = $info[0]['sn'][0];
                            $display_name = $info[0]['displayname'][0];
                            $ldap_logged_in = true;  
                        }
                    }
                        
                    @ldap_unbind($ad);
                    
                    if($ldap_logged_in === true)
                    {
                        if($this->ci->ion_auth_model->username_check($this->ci->input->post('email')))
                        {
                            // Re-hash user password in PyroCMS
                            $user = $this->ci->db->get_where(SITE_REF.'_users', array('username' => $this->ci->input->post('email')))->result();
                            foreach($user as $v)
                            {
                                $salt = $v->salt;
                                break;
                            }
                            $this->ci->db->where('username', $this->ci->input->post('email'));
                            $data = array(
                                'password' => $this->ci->ion_auth_model->hash_password($this->ci->input->post('password'),$salt)
                            );
                            $this->ci->db->update(SITE_REF.'_users', $data);

                            // Return data to system admin controller
                            $cred = array(
                                'username' => $this->ci->input->post('email'),
                                'password' => $this->ci->input->post('password')
                            );
                            return json_encode($cred);
                            break;
                        }
                        else
                        {
                            // If already a user, try to log them in using the PyroCMS database instead of LDAP
                            $this->ci->db->where('username',$this->ci->input->post('email'));
                            $this->ci->db->from(SITE_REF.'_users');
                            $is_account = $this->ci->db->count_all_results();

                            if($is_account)
                            {
                                $cred = array(
                                    'username' => $this->ci->input->post('email'),
                                    'password' => $this->ci->input->post('password')
                                );
                                return json_encode($cred);
                                break;
                            }
                            else
                            {
                                $try = $this->ci->ion_auth_model->register($this->ci->input->post('email'), $this->ci->input->post('password'), $this->ci->input->post('email'), $group, $additional_data = array('first_name' => $first_name, 'last_name' => $last_name, 'display_name' => $display_name));

                                if($try)
                                {
                                    $cred = array(
                                        'username' => $this->ci->input->post('email'),
                                        'password' => $this->ci->input->post('password')
                                    );
                                    return json_encode($cred);
                                    break;
                                } 
                            }
                        }
                    }
                }
                else
                {
                    // ldap login failed, ion will try to handle the login
                    @ldap_unbind($ad);
                }
            }

            // Can't login because cred doesn't match
            return false;
        }

        // Your settings aren't in PyroCMS
        return false;

    }
    
}
/* End of file events.php */